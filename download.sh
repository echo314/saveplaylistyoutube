#!/bin/bash

# name			: download.sh
# author(s)		: "echo" <dev@lointaine.fr>
# Creation date	: 2020-02-07
# Licence		: GPL v3 https://www.gnu.org/licenses/gpl-3.0.txt
# repository	: https://gitlab.com/echo314/saveplaylistyoutube
# version		: 1.0
# Subject 		: recuperer les video de playlist en vu de les sauvegarder sur la machine 


FIC_LST="./playlist.txt"
FIC_TMP="./playlist.tmp"
FIC_DOWNLOAD_LST="./download.lst"

[[ -f $FIC_TMP ]] && rm -rf $FIC_TMP
cp $FIC_LST $FIC_TMP

sed -i '/^#/d' $FIC_TMP
sed -i '/^$/d' $FIC_TMP

# on lit le fichier temporaire
cat $FIC_TMP | while  read ligne ; do

	URL=$(echo $ligne |cut -d";" -f2)
	TITLE=$(echo $ligne |cut -d";" -f1)

	echo "------ " $TITLE "------"

	# on cherche dans la page si il y a des playlist
	curl -s  "${URL}/playlists" |grep "/playlist?list="|grep "yt-lockup-title" | cut -d"=" -f10 |cut -d'"' -f1 >> $FIC_DOWNLOAD_LST

done

# on lit le fichier des ID de playlist
cat $FIC_DOWNLOAD_LST | while  read ligne ; do

	ID_PLAYLIST=$(echo $ligne |cut -d";" -f2)

# on telecharge
	youtube-dl  	-o '%(playlist_uploader)s/%(playlist_title)s/%(playlist_index)s_%(upload_date)s_%(title)s.%(ext)s' \
			-cwi \
			--write-description \
			--write-info-json \
			--write-annotations \
			--write-sub \
			--write-thumbnail \
			--restrict-filenames \
			-f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]' \
			--merge-output-format mp4 \
			--download-archive archive.txt \
			https://www.youtube.com/playlist?list=$ID_PLAYLIST

done


[[ -f $FIC_TMP ]] && rm -rf $FIC_TMP
[[ -f $FIC_DOWNLOAD_LST ]] && rm -rf $FIC_DOWNLOAD_LST

#
exit
